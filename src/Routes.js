import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from "./App";
import Profile from "./Profile";
import AboutUs from "./AboutUs";

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/aboutus" component={AboutUs} />
        <Route path="/profile" component={Profile} />
        <Route path="/" component={App} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
